import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { Store } from './store';
import  Routes  from './Routes';
import './style.css';
ReactDOM.render(
  <Provider store={Store}>
    <Routes/>
  </Provider>
, document.getElementById('root'));
