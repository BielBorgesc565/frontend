import {
    SET_VALUE_EMAIL,
    SET_VALUE_PASSWORD,
    CLICK_BUTTON_LOGIN,
    SUCCESS_LOGIN
} from './actionTypes';

export const setValueEmail = (value) => ({
    type: SET_VALUE_EMAIL,
    payload: value
});

export const setValuePassword = (value) => ({
    type: SET_VALUE_PASSWORD,
    payload: value
});

export const clickButtonLogin = () => ({
    type: CLICK_BUTTON_LOGIN
});

export const updateSuccess = (bool) => {
    return({
        type: SUCCESS_LOGIN,
        payload: bool
    })
    
};