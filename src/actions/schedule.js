import {GET_EVENTS_LIST, SET_EVENTS_LIST,  CLICK_BUTTON_DELETE, SET_ID_REMOVE} from './actionTypes'

export const getEventsList = () => ({
    type: GET_EVENTS_LIST,
});

export const setEventsList = (array) => ({
    type: SET_EVENTS_LIST,
    payload: array
});

export const clickButtonDelete = () => ({
    type: CLICK_BUTTON_DELETE,
});

export const setIdRemove = (id) => ({
    type: SET_ID_REMOVE,
    payload: id
});