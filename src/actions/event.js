import { 
    CLICK_BUTTON_SEND,
    SHOW_ALERT,
} from "./actionTypes";

export const clickButtonSend = (values) => ({
    type: CLICK_BUTTON_SEND,
    payload: values
});

export const showAlert = (info) => ({
    type: SHOW_ALERT,
    payload: info
});
