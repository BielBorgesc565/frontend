import {
    ACTIVATE_DRAWER
} from './actionTypes';

export const setOpenDrawer = (bool) => ({
    type: ACTIVATE_DRAWER,
    payload: bool
});