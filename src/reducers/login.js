import { 
  SET_VALUE_EMAIL,
  SET_VALUE_PASSWORD,
  SUCCESS_LOGIN,
} from '../actions/actionTypes'

const initialState = {
  email: 'borges565@gmail.com',
  password: 'gabriel',
  login: false
};

export const login = (state = initialState, action) => {
  switch (action.type) {

    case SET_VALUE_EMAIL:
      return{
        ...state,
        email: action.payload
      }
      
    case SET_VALUE_PASSWORD:
      return{
        ...state,
        password: action.payload
      }

    case SUCCESS_LOGIN:
      return{
        ...state,
        login: action.payload
      }
    default:
      return state;
  }
};