import { 
    ACTIVATE_DRAWER
  } from '../actions/actionTypes';
  
  const initialState = {
    openDrawer: false
  };
  
  export const main = (state = initialState, action) => {
    switch (action.type) {
  
      case ACTIVATE_DRAWER:
        return{
          ...state,
          openDrawer: action.payload
        }
      default:
        return state;
    }
  };