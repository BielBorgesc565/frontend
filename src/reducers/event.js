import { 
    SHOW_ALERT,
} from '../actions/actionTypes';
  
  const initialState = {
    showAlert: false,
  };
  
  export const event = (state = initialState, action) => {
    switch (action.type) {
      case SHOW_ALERT:
        return{
            ...state,
            showAlert: action.payload
        }

      default:
        return state;
    }
  };