import {SET_EVENTS_LIST, SET_ID_REMOVE} from '../actions/actionTypes';

const initialState = {
    eventsList: [],
    idRemove: ''
}

export const schedule = (state = initialState, action) => {
    switch(action.type){
        case SET_EVENTS_LIST:
            return{
                ...state,
                eventsList: action.payload
            }

        case SET_ID_REMOVE:
        return{
            ...state,
            idRemove: action.payload
        }

        default:
            return state
    }
}