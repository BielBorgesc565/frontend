import { combineReducers } from 'redux';
import {login} from './login';
import {schedule} from './schedule';
import {main} from './main';
import {event} from './event';

export const Reducers = combineReducers({
  login: login,
  schedule: schedule,
  main: main,
  event: event
});
