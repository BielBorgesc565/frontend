import { call, takeEvery, put} from "redux-saga/effects";
import axios from 'axios';
import {CLICK_BUTTON_SEND} from '../actions/actionTypes';
import {showAlert} from '../actions/event';

const creatEvent = async(formValues) =>{
    const token = localStorage.getItem("isAuthenticated");
    return await axios.post("http://localhost:4000/event",
    {
        description: formValues.description,
        init_date: formValues.init_date,
        final_date: formValues.final_date,
    },
    {
        headers : {
            "Authorization" : "Bearer "+token
        }
    })
    .then(response => response)
    .catch(error => error)
}

function* sendEvent(actionResult){
    try{
        const formValues = actionResult.payload.formValues;
        yield call(() => creatEvent(formValues));
        yield put(showAlert(true));
    }catch(e){
        yield call(() => console.error('ERRO: ', e));
        yield call(() => showAlert('erro'));
    }
}

export function* sendEventSaga(){
    yield takeEvery(CLICK_BUTTON_SEND, sendEvent);
}

