import { call, takeEvery, select, put} from "redux-saga/effects";
import axios from 'axios'
import {
    CLICK_BUTTON_LOGIN,
} from '../actions/actionTypes';
import { updateSuccess } from "../actions/login";

const verifyLoginInAPI = async(email,password) =>{
    return await axios.post("http://localhost:4000/user/login",{
        email: email,
        password: password
    })
    .then(response => response)
    .catch(error => error)
}

function* confirmDataLogin(){
    try{
        const email = yield select(state => state.login.email);
        const password = yield select(state => state.login.password);
        const result = yield call(() => verifyLoginInAPI(email, password));
        if(result.status === 200){
            localStorage.setItem("isAuthenticated",result.data.token);
            yield put(updateSuccess(true));
        }
    }catch(e){
        yield call(() => console.error('ERRO: ', e));
    }
}

export function* confirmDataLoginSaga(){
    yield takeEvery(CLICK_BUTTON_LOGIN, confirmDataLogin);
}

