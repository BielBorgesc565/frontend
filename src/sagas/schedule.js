import {call, put, takeEvery, select} from "redux-saga/effects";
import axios from 'axios';
import {
    GET_EVENTS_LIST,
    CLICK_BUTTON_DELETE
} from '../actions/actionTypes';
import { setEventsList } from "../actions/schedule";

const getEventsListAPI = async () => {
    return await axios.get("http://localhost:4000/event")
    .then(response => response)
    .catch(error => error)
}

function* getEventsList(){
    try{
        const result = yield call(() => getEventsListAPI());
        if(result.status === 200){
            yield put(setEventsList(result.data.events));

        }
    }catch(e){
        yield call(() => console.error('ERRO: ', e));
    }
}

const deleteEvent = async (id) => {
    const token = localStorage.getItem("isAuthenticated")
    return await axios.delete(`http://localhost:4000/event/${id}`,
    {
        headers : {
            "Authorization" : "Bearer "+token
        }
    })
    .then(response => response)
    .catch(error => error)
}

function* clickButtonDelete(){
    const id = yield select(state => state.schedule.idRemove);
    const result = yield call(() => deleteEvent(id));
    if(result.status === 202){
        yield call(() => getEventsList());
    }
    
}

export function* clickButtonDeleteSaga(){
    yield takeEvery(CLICK_BUTTON_DELETE, clickButtonDelete);
}

export function* getEventsListSaga(){
    yield takeEvery(GET_EVENTS_LIST, getEventsList);
}