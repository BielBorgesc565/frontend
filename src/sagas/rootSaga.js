import {all, fork} from "redux-saga/effects";
import {confirmDataLoginSaga} from './login'
import {sendEventSaga} from './event'
import {getEventsListSaga, clickButtonDeleteSaga} from './schedule'

function* rootSaga() {
    yield all([
        fork(confirmDataLoginSaga),
        fork(sendEventSaga),
        fork(getEventsListSaga),
        fork(clickButtonDeleteSaga)
    ])

}
export default rootSaga;