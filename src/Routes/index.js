import React from 'react';
import { Route, BrowserRouter, Switch, Redirect } from "react-router-dom";
import Login from '../components/Login/index';
import Event from '../components/Event/index';
import Schedule from '../components/Schedule/index';
import {isAuthenticated} from './auth';

const PrivateRoute = ({component: Component, ...rest}) => (
    <Route {...rest} render={props => (
        isAuthenticated() ? (
            <Component {...props} /> 
        ) : (
            <Redirect to={{pathname: '/login'}}/>
        )
    )}/>
)

const Routes = () => (
    <BrowserRouter>
        <Switch>
            <PrivateRoute exact path="/" component={Schedule} />
            <Route  path="/login" component={Login}/>
            <PrivateRoute  path="/event" component={Event}/>
        </Switch>
    </BrowserRouter>
);

export default Routes;