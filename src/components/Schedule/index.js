import React, { Component } from 'react';
import { connect } from 'react-redux';
import Main from '../Main/index';
import { getEventsList, clickButtonDelete,  setIdRemove} from '../../actions/schedule'
import AccessTimeIcon from '@material-ui/icons/AccessTime';
import EventIcon from '@material-ui/icons/Event';
import {
  Container,
  Card,
  CardContent,
  Typography,
  Button
} from '@material-ui/core';


class Schedule extends Component {

  constructor(props) {
    super(props)
    this.state = {
      eventsList: []
    }
  }

  componentDidMount = () => {
    this.props.dispatchGetEventsList();
  }

  handleFormatTime(element){
    let initTime = (element.init_date.split("T")[1]).split(".")[0];
    let finalTime = (element.final_date.split("T")[1]).split(".")[0];
    return initTime + " - " + finalTime
  }

  handleFormatDate(element){
    let initTime = (element.init_date.split("T")[0]).split("-").reverse().join("/");
    let finalTime = (element.final_date.split("T")[0]).split("-").reverse().join("/");
    if(initTime == finalTime) return initTime;
    return initTime + " até " + finalTime;
  }

  handleDeleteEvent(id){
    this.props.dispatchSetEventRemove(id);
    this.props.dispatchClickButtonDelete();
  }

  render() {
    return (
      <div>
        <Main />
        <div style={{ display: "flex", justifyContent: "center" }}>
          <h1>Seus Eventos</h1>
        </div>
        <Container style={{ display: "flex", alignItems: "center", justifyContent: "center", flexWrap: "wrap" }}>
          {this.props.eventsList.map((element, index) => {
            return (
              <Card style={{ margin: "20px", backgroundColor: "#1A2F3A", color: "white", width: "250px", height: "200px" }} key={index}>
                <CardContent>
                  <Typography color="initial" gutterBottom>
                    {element.description}
                  </Typography>
                  <Typography variant="body1" component="p" style={{display: "flex", margin: "10px"}}>
                    <AccessTimeIcon style={{marginRight: "10px"}}/>{this.handleFormatTime(element)}
                  </Typography>
                  <Typography variant="body1" component="p" style={{display: "flex", margin: "10px"}}>
                    <EventIcon style={{marginRight: "10px"}}/>  {this.handleFormatDate(element)}
                  </Typography>
                </CardContent>
                <div style={{float: "right", margin: "5px"}} id={element.id_event}>
                  <Button color="secondary" id={element.id_event} onClick={() => this.handleDeleteEvent(element.id_event)}>Excluir</Button>
                </div>
              </Card>
            )
          })}
        </Container>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  eventsList: state.schedule.eventsList
});


const mapDispatchToProps = dispatch => {
  return {
    dispatchGetEventsList: () => dispatch(getEventsList()),
    dispatchSetEventRemove: (id) => dispatch(setIdRemove(id)),
    dispatchClickButtonDelete: () => dispatch(clickButtonDelete())
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Schedule);
