import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
  AppBar,
  Toolbar,
  Typography,
  Button,
  IconButton,
  Drawer,
  ListItemIcon,
  ListItemText,
  ListItem,
} from '@material-ui/core'
import MenuIcon from '@material-ui/icons/Menu';
import ExitToAppIcon from '@material-ui/icons/ExitToApp';
import EventNoteIcon from '@material-ui/icons/EventNote';
import AddIcon from '@material-ui/icons/Add';
import imageLogoLeters from '../../image/lettersLogo.png';
import imageLogoSquare from '../../image/squareLogotipo.png';
import {setOpenDrawer} from '../../actions/main';
import { updateSuccess } from "../../actions/login";
import { withRouter } from 'react-router';

class Main extends Component {

  constructor(props){
    super(props)
    this.state = {
      openDrawer: false
    }
  }
  
  handleOpenDrawer = (event) => {
    this.props.dispatchSetOpenDrawer(true);
  }

  haldleCloseDrawer = (event) => {
    this.props.dispatchSetOpenDrawer(false);
  }

  handleExitAccount = () => {
    localStorage.clear();
    this.props.dispatchSetLogout(false);
  }


  render() {
    return (
      <div>
        <AppBar position="static">
          <Toolbar style={{display: "flex", 
            justifyContent: "space-between", 
            alignItems: "center",
            backgroundColor: "#1A2F3A",}}>
            <IconButton edge="start"  color="inherit" aria-label="menu" onClick={this.handleOpenDrawer}>
              <MenuIcon />
            </IconButton>
              <Drawer anchor={'left'} 
                open={this.props.openDrawer}
                onClose={this.haldleCloseDrawer}>
                <img src={imageLogoSquare} alt="Imagem com calendário e uma breve descrição" style={{maxWidth: "200px"}}/>
                <Button>
                  <ListItem style={{padding: "0"}}>
                        <ListItemIcon style={{minWidth: "30px"}}><EventNoteIcon/></ListItemIcon>
                        <a href="/" style={{textDecoration: "none", color: "black"}}><ListItemText primary={'Cronograma'}/></a>
                  </ListItem>
                </Button>
                <Button>
                  <ListItem style={{padding: "0"}}>
                        <ListItemIcon style={{minWidth: "30px"}}><AddIcon/></ListItemIcon>
                        <a href="/event" style={{textDecoration: "none", color: "black"}}><ListItemText primary={'Agendar Evento'}/></a>
                  </ListItem>
                </Button>
              </Drawer>
            <Typography variant="h6">
              <img src={imageLogoLeters} alt="Imagem com calendário e o nome do site" style={{height: "40px", width: "190px", marginTop: "10px"}}/>
            </Typography>
            <Button color="inherit" onClick={() => this.handleExitAccount()}> <ExitToAppIcon/> </Button>
          </Toolbar>
        </AppBar>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  openDrawer: state.main.openDrawer,
  login: state.login.login
});


const mapDispatchToProps = dispatch =>{
  return {
    dispatchSetOpenDrawer: (bool) => dispatch(setOpenDrawer(bool)),
    dispatchSetLogout: (bool) => dispatch(updateSuccess(bool))
  }
}
export default (withRouter,connect(mapStateToProps, mapDispatchToProps))(Main);