import React, { Component } from 'react';
import { connect } from 'react-redux';
import Main from '../Main/index';
import { 
  clickButtonSend,
  showAlert
} from '../../actions/event';
import {
   TextField,
   Box,
   Button,
   Grid,
   MuiThemeProvider, 
   createTheme,
} from '@material-ui/core';
import Alert from '@material-ui/lab/Alert';

const theme = createTheme({
  palette: {
    primary: {
      main: "#1A2F3A",
      contrastText: "#fff",
    },
  },
});

class Event extends Component {

  constructor(props){
    super(props)
    this.state = {
      description: '',
      initDate: '',
      finalDate: '',
      initTime: '',
      finalTime: ''
    }
  };


  handleClickButtonSubmit = () => {
    if(this.state.initDate === "" ||
      this.state.initTime === "" || 
      this.state.finalDate === "" ||
      this.state.finalTime === "" ||
      this.state.description === ""){

        this.props.dispatchShowAleter("warning");

    }else{
      const init_date = this.state.initDate + " " + this.state.initTime;
      const final_date = this.state.finalDate + " " + this.state.finalTime;
  
      this.props.dispatchSetEvent({
        formValues: {
          description: this.state.description,
          init_date: init_date,
          final_date: final_date,
        }
      });
  
      this.setState((state) => {
        return {
          description : "",
          initDate : "",
          finalDate : "",
          initTime : "",
          finalTime : ""
        }
      })
    }
  }

  componentDidUpdate = () => {
    if(this.props.showAlert === true){
      setTimeout(() => this.props.dispatchShowAleter(false),5000);
    }
    if(this.props.showAlert === 'erro'){
      setTimeout(() => this.props.dispatchShowAleter(false),5000);
    }
    if(this.props.showAlert === 'warning'){
      setTimeout(() => this.props.dispatchShowAleter(false),5000);
    }
  }

  handleUpdateForm = (event, type) => {
    switch(type){
      case "description":
        return this.setState({description: event.target.value});

      case "initDate":
        return this.setState({initDate: event.target.value});

      case "finalDate":
        return this.setState({finalDate: event.target.value});

      case "initTime":
        return this.setState({initTime: event.target.value});

      case "finalTime":
        return this.setState({finalTime: event.target.value});
      
      default:
        return 0;
    }
  }

  render() {
    return (
        <MuiThemeProvider theme={theme}>
            <Main/>
            <Grid>
              <Box style={{width: "50vw", marginLeft: "auto", marginRight: "auto", 
                          marginTop: "50px", display: "flex", flexDirection:"column",
                          border: "#1A2F3A solid 3px", padding: "50px", borderRadius: "10px"}}>
                <div style={{alignItems: "center", display: "flex", marginBottom:"10px"}}>
                  <label>Descrição:</label>
                  <TextField placeholder="Insira uma descrição" value={this.state.description}  style={{margin:"10px", width:"420px"}}  onChange={(value) => this.handleUpdateForm(value, "description")}/>
                </div>
                <div style={{alignItems: "center", display: "flex", marginBottom:"10px"}}  >
                  <label>Data Inicial</label>
                  <TextField  type="date" style={{margin:"10px"}} onChange={(value) => this.handleUpdateForm(value, "initDate")} value={this.state.initDate}/>
                  <label>Data Final</label>
                  <TextField type="date" style={{margin:"10px"}} onChange={(value) => this.handleUpdateForm(value, "finalDate")} value={this.state.finalDate}/>
                </div>
                <div style={{alignItems: "center", display: "flex", marginBottom:"10px"}}>
                  <label>Horário Inicial</label>
                  <TextField  type="time" style={{margin:"10px"}} onChange={(value) => this.handleUpdateForm(value, "initTime")} value={this.state.initTime}/>
                  <label>Horário Final</label>
                  <TextField  type="time" style={{margin:"10px"}} onChange={(value) => this.handleUpdateForm(value, "finalTime")} value={this.state.finalTime}/>
                </div>
                <div style={{display: "flex", flexDirection: "row-reverse"}}>
                  <Button  
                  variant="contained" 
                  color="primary" 
                  onClick = {this.handleClickButtonSubmit}>Enviar</Button>
                </div>
              </Box>
            </Grid>
            {this.props.showAlert === true && 
              <div style={{position: "absolute", top: "60px", right: 0}}>
                <Alert severity="success">Dados Enviados com sucessos</Alert>
              </div>
            }
            {this.props.showAlert === 'erro' && 
              <div style={{position: "absolute", top: "60px", right: 0}}>
                <Alert severity="error">Ocorreu uma falha ao enviar os dados</Alert>
              </div>
            }
            {this.props.showAlert === 'warning' && 
              <div style={{position: "absolute", top: "60px", right: 0}}>
                <Alert severity="warning">Por favor prencher todos os campos</Alert>
              </div>
            }
        </MuiThemeProvider>
    );
  }
}

const mapStateToProps = state => ({
  showAlert: state.event.showAlert
});


const mapDispatchToProps = dispatch =>{
  return {
    dispatchSetEvent: (formValues) => dispatch(clickButtonSend(formValues)),
    dispatchShowAleter: (info) => dispatch(showAlert(info))
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Event);
