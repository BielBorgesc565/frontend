import React, { Component } from 'react';
import { connect } from 'react-redux';
import { setValueEmail, setValuePassword, clickButtonLogin } from '../../actions/login';
import { withRouter } from 'react-router';
import {
    Grid,
    Avatar,
    TextField,
    Button,
    Box,
    MuiThemeProvider, 
    createTheme
} from '@material-ui/core';
import PersonIcon from '@material-ui/icons/Person';
import {updateSuccess} from '../../actions/login';

const theme = createTheme({
  palette: {
    primary: {
      main: "#1A2F3A",
      contrastText: "#fff",
    },
  },
});

class Login extends Component {

  constructor(props){
    super(props)
    this.state = {
      email: '',
      password: ''
    }
  }

  handleChangeInputEmail = event => {
    this.props.dispatchSetEmail(event.target.value);
  }

  handleChangeInputPassword = event => {
    this.props.dispatchSetPassword(event.target.value);
  }

  handleClickButtonLogin = () => {
    this.props.dispatchClickButtonLogin();
    this.props.history.push("/");
  }

  componentDidUpdate = () => {
    this.handleClickButtonLogin();
  }

  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <Box style={{ height: "100vh", width: "100vw", display: "flex", flexDirection: "row",
                      justifyContent: "center", alignItems: "center", backgroundColor: "#1A2F3A"}}>
          <Grid
            container
            direction="column"
            alignItems="center"
            style={{backgroundColor: "white", width: "30vw", height: "50vh", borderRadius: "10px"}}
          >
            <Avatar style={{margin: "10px", backgroundColor: "#1A2F3A"}}>
              <PersonIcon />
            </Avatar>
              <TextField 
                label="Email" 
                variant="outlined" 
                style={{margin:"10px"}} 
                required={true} 
                placeholder="Informe seu email" 
                type="email"
                onChange={this.handleChangeInputEmail}
                value={this.props.email}
                />
              <TextField 
                label="Password" 
                variant="outlined" 
                style={{margin:"10px"}} 
                required={true} 
                placeholder="Informe sua senha" 
                type="password"
                onChange={this.handleChangeInputPassword}
                value={this.props.password}
                />
              <Button 
                variant="contained" 
                color="primary" 
                style={{margin:"10px"}}
                onClick={() => this.handleClickButtonLogin()}>Entrar</Button>
          </Grid>
        </Box>
      </MuiThemeProvider>
    );
  }
}

const mapStateToProps = state => ({
  email: state.login.email,
  password: state.login.password,
  login: state.login.login
});


const mapDispatchToProps = dispatch =>{
  return {
    dispatchSetEmail: (value) => dispatch(setValueEmail(value)),
    dispatchSetPassword: (value) => dispatch(setValuePassword(value)),
    dispatchClickButtonLogin: () => dispatch(clickButtonLogin()),
    dispatchUpdateLoginState: (info) => dispatch(updateSuccess(info))
  }
}
export default (withRouter, connect(mapStateToProps, mapDispatchToProps))(Login);
